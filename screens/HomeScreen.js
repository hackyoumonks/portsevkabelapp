import React, { PureComponent } from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    View,
    Dimensions,
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Body,
    Card,
    CardItem,
    Thumbnail,
    Button,
    Icon,
    Left,
} from 'native-base';
import Carousel from 'react-native-looped-carousel';
import * as Expo from 'expo';

const { width, height } = Dimensions.get('window');

export default class HomeScreen extends PureComponent {
    static navigationOptions = {
        header: null,
    };

    _renderItem({ item }) {
        return (
            <View style={styles.slide}>
                <Text style={styles.title}>{item.title}</Text>
            </View>
        );
    }

    render() {
        return (
            <Container>
                <Content>
                    <CarouselMain />
                    <View style={{ marginTop: 10 }}>
                        <Text style={{ fontSize: 30 }}>Горячее событие</Text>
                        <Image
                            source={require('../assets/images/fest.png')}
                            style={{
                                marginLeft: 'auto',
                                marginRight: 'auto',
                                width: '90%',
                            }}
                        />
                        <Button transparent success>
                            <Text>
                                Подробнее <Icon name="md-arrow-dropright" />
                            </Text>
                        </Button>
                    </View>
                    <Text style={{ fontSize: 30 }}>Топ мероприятий</Text>
                    <ScrollView horizontal={true}>
                        <TopEvents eventscards={cards} />
                    </ScrollView>
                    <Text style={{ fontSize: 30 }}>Пространство</Text>
                    <ScrollView horizontal={true}>
                        <Places placescards={places} />
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}

class CarouselMain extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            size: { width, height },
        };
    }

    _onLayoutDidChange = e => {
        const layout = e.nativeEvent.layout;
        this.setState({ size: { width: layout.width, height: layout.height } });
    };

    render() {
        return (
            <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>
                <Carousel
                    delay={4000}
                    style={this.state.size}
                    autoplay
                    pageInfo
                    //onAnimateNextPage={p => console.log(p)}
                >
                    <View
                        style={[
                            { backgroundColor: '#BADA55' },
                            this.state.size,
                        ]}
                        onPress={() => this.props.navigation.navigate('Event')}
                    >
                        <Image
                            style={styles.image}
                            source={require('../assets/images/carousel/1.png')}
                        />
                    </View>
                    <View
                        style={[{ backgroundColor: 'red' }, this.state.size]}
                        
                    >
                        <Image
                            style={styles.image}
                            source={require('../assets/images/carousel/2.jpg')}
                        />
                    </View>
                    <View
                        style={[{ backgroundColor: 'blue' }, this.state.size]}
                    >
                        <Image
                            style={styles.image}
                            source={require('../assets/images/carousel/1.png')}
                        />
                    </View>
                </Carousel>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        //width: '100%',
        height: '100%',
    },
});

class TopEvents extends PureComponent {
    render() {
        return this.props.eventscards.map((item, i) => (
            <Card style={{ flex: 1, height: 360, width: 300 }} key={i}>
                <CardItem bordered>
                    <Left>
                        <Thumbnail
                            source={
                                item.avatar
                                    ? { uri: item.avatar }
                                    : require('../assets/images/avatarph.png')
                            }
                        />
                        <Body>
                            <Text>{item.title}</Text>
                            <Text note>{`${item.time.hh}:${item.time.mm} ${
                                item.time.dd
                            }.${item.time.MM}.${item.time.yyyy}`}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem bordered>
                    <Body>
                        <Image
                            source={
                                item.imgHref
                                    ? { uri: item.imgHref }
                                    : require('../assets/images/eventph.png')
                            }
                            style={{ height: 150, width: 300 }}
                        />
                        <Text>{item.description}</Text>
                    </Body>
                </CardItem>
                <CardItem bordered>
                    <Left>
                        <Button transparent textStyle={{ color: '#87838B' }}>
                            <Icon name="star" />
                            <Text>{item.stars} stars</Text>
                        </Button>
                    </Left>
                </CardItem>
            </Card>
        ));
    }
}

class Places extends PureComponent {
    constructor(props) {
        super(props);
        this.state = { loading: true };
    }

    async componentWillMount() {
        await Expo.Font.loadAsync({
            Roboto: require('native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
            Ionicons: require('@expo/vector-icons/fonts/Ionicons.ttf'),
        });
        this.setState({ loading: false });
    }

    render() {
        if (this.state.loading) {
            return <Expo.AppLoading />;
        }

        return this.props.placescards.map((item, i) => (
            <Card style={{ flex: 1, height: 360, width: 300 }} key={i}>
                <CardItem bordered>
                    <Left>
                        <Thumbnail
                            source={
                                item.avatar
                                    ? { uri: item.avatar }
                                    : require('../assets/images/avatarph.png')
                            }
                        />
                        <Body>
                            <Text>{item.title}</Text>
                            <Text note>{`${item.time.hh}:${item.time.mm} ${
                                item.time.dd
                            }.${item.time.MM}.${item.time.yyyy}`}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem bordered>
                    <Body>
                        <Image
                            source={
                                item.imgHref
                                    ? { uri: item.imgHref }
                                    : require('../assets/images/eventph.png')
                            }
                            style={{ height: 150, width: 300 }}
                        />
                        <Text>{item.description}</Text>
                    </Body>
                </CardItem>
                <CardItem bordered>
                    <Left>
                        <Button transparent textStyle={{ color: '#87838B' }}>
                            <Icon name="star" />
                            <Text>{item.stars} stars</Text>
                        </Button>
                    </Left>
                </CardItem>
            </Card>
        ));
    }
}

const cards = [
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
];

const places = [
    {
        title: 'Поесть',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Поспать',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Поиграть',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Подмать',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
];
