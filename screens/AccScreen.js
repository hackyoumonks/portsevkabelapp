import React, { PureComponent } from 'react';


import {
    View,
} from 'react-native';

import {
    Container,
    Content,
    Text,
    Body,
    Card,
    CardItem,
    Button,
    Icon,
    Left,
    Right,
    Header,
} from 'native-base';

import {
    Collapse,
    CollapseHeader,
    CollapseBody,
} from 'accordion-collapse-react-native';

import { Calendar } from 'react-native-calendars';


const cards = [
    {
        title: 'Атриум',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        status: 0,
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Площадка',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        status: 1,
        imgHref: '',
        avatar: '',
        stars: 34,
    },
];

export default class AccScreen extends PureComponent {
    static navigationOptions = {
        header: 
        <Header>
            <Body>
                <Text>
                'Личный кабинет'
                </Text>
            </Body>
            <Right>
                <Icon name="md-flash" />
            </Right>
        </Header>,
   
    };

    constructor(props) {
        super(props);
        this.state = {
            collapsed: false,
        };
    }

    render() {
        return (
            <Container >
                <Content>
                    <View
                        style={{
                            width: '80%',
                            marginTop: 10,
                            marginBottom: 10,
                            marginLeft: 'auto',
                            marginRight: 'auto',
                        }}
                    >
                        <Text style={{ fontSize: 30, fontWeight: 'bold' }}>
                            Юля Зигитбаева
                        </Text>
                        <Text style={{ fontSize: 20, fontStyle: 'normal' }}>
                            Организатор
                        </Text>
                    </View>

                    <Calendar
                    markedDates={
                        {'2018-11-11': {textColor: 'green'},
                         '2018-11-15': {startingDay: true, color: 'green'},
                         '2018-11-16': {selected: true, endingDay: true, color: 'green', textColor: 'gray'},
                         '2018-11-20': {disabled: true, startingDay: true, color: 'green', endingDay: true}
                        }}
                      // Date marking style [simple/period/multi-dot/custom]. Default = 'simple'
                      markingType={'period'} />

                    <View>
                        <Collapse
                            //style={{ flexDirection: 'row' }}
                            isCollapsed={this.state.collapsed}
                            onToggle={isCollapsed => {
                                this.setState({ collapsed: isCollapsed });
                                console.log(isCollapsed);
                            }}
                        >
                            <CollapseHeader>
                                <View style={{ height: 50, width: '90%', backgroundColor: '#FEE17F99', margin: 20, paddingLeft: 20, paddingRight: 20, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center'}}>
                                    <Text style={{fontSize: 18}}>Заявки</Text>
                                    {this.state.collapsed ? <Icon name="md-arrow-dropdown" /> :<Icon name="md-arrow-dropright" />  }
                                </View>
                            </CollapseHeader>
                            <CollapseBody
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    padding: 10,
                                }}
                            >
                                <Applications cards={cards} />
                            </CollapseBody>
                        </Collapse>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around', margin: 20}}>
                        <Button >
                            <Text>Забронировать</Text>
                        </Button>

                        <Button onPress={() => alert('Вы вышли!')}>
                            <Text>Выйти</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

class Applications extends PureComponent {
    render() {
        return this.props.cards.map((item, i) => (
            <Card style={{ flex: 1, width: 250 }} key={i}>
                <CardItem bordered>
                    <Left>
                        <Body>
                            <Text>{item.title}</Text>
                            <Text note>{`${item.time.hh}:${item.time.mm} ${
                                item.time.dd
                            }.${item.time.MM}.${item.time.yyyy}`}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem bordered>
                    <Body>
                        <Text>{item.status == 0 ? 'Новая' : item.status == 1 ? 'На рассмотрении' : item.status == 2 ? 'Принята' : 'Отклонена'}</Text>
                    </Body>
                </CardItem>
                <CardItem bordered>
                    <Right>
                        <Button transparent textStyle={{ color: '#87838B' }}>
                            <Text>Связаться</Text>
                            <Icon name="md-link" />
                        </Button>
                    </Right>
                </CardItem>
            </Card>
        ));
    }
}
