import React from 'react';
import {
    Image,
    ScrollView,
    View,
} from 'react-native';
import {
    Text,
} from 'native-base';

export default class AboutScreen extends React.Component {
    static navigationOptions = {
        title: 'О порте Севкабель',
    };

    render() {
        /* Go ahead and delete ExpoConfigView and replace it with your
         * content, we just wanted to give you a quick view of your config */
        return (
            <ScrollView>
                <View
                    style={{
                        width: '80%',
                        marginTop: 10,
                        marginBottom: 10,
                        marginLeft: 'auto',
                        marginRight: 'auto',
                    }}
                >
                    <Text style={{ fontSize: 30, fontWeight: 'bold' }}>
                        Порт Севкабель
                    </Text>
                    <Text style={{ fontSize: 15, fontWeight: 'normal' }}>
                        КОЖЕВЕННАЯ ЛИНИЯ 40, ГАВАНЬ В.О., САНКТ-ПЕТЕРБУРГ
                    </Text>
                </View>
                <View style={{
                        width: '90%',
                        marginTop: 10,
                        marginBottom: 10,
                        marginLeft: 'auto',
                        marginRight: 'auto',
                    }}>
                <Text style={{ fontSize: 15, fontWeight: 'normal' }}>
                    Порт Севкабель - это проект преобразования исторического
                    "серого пояса" в полифункциональное общественно-деловое
                    пространство, открывающего новую морскую набережную
                    Санкт-Петербурга.
                </Text>
                <Image
                    source={require('../assets/images/about/1.png')}
                    style={{
                        margin: 15,
                        width: '90%',
                    }}
                />
                <Text style={{ fontSize: 15, fontWeight: 'normal' }}>
                    Вход на территорию креативного кластера свободен с 11.00 до
                    23.00 часов
                </Text>
                <Image
                    source={require('../assets/images/about/2.png')}
                    style={{
                        margin: 15,
                        width: '90%',
                    }}
                />
                <Image
                    source={require('../assets/images/about/3.png')}
                    style={{
                        margin: 15,
                        width: '90%',
                    }}
                />

                </View>
            </ScrollView>
        );
    }
}
