import React, { PureComponent } from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    View,
} from 'react-native';
import {
    Text,
    Body,
    Card,
    CardItem,
    Thumbnail,
    Button,
    Icon,
    Left,
} from 'native-base';

const cards = [
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
    {
        title: 'Фестиваль музыки',
        time: { hh: 11, mm: 0, dd: 14, MM: 11, yyyy: 2018 },
        description: 'Самое крутое событие осени! БИ-2, Звери, Muse и другие',
        imgHref: '',
        avatar: '',
        stars: 34,
    },
];

export default class ScheduleScreen extends React.Component {
    static navigationOptions = {
        title: 'Расписание',
    };

    render() {
        return (
            <ScrollView style={styles.container}>
                {/* Go ahead and delete ExpoLinksView and replace it with your
                 * content, we just wanted to provide you with some helpful links */}
                <View style={{alignItems: 'center'}}>
                    <TopEvents eventscards={cards} />

                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});

class TopEvents extends PureComponent {
    render() {
        return this.props.eventscards.map((item, i) => (
            <Card style={{ flex: 1, height: 360, width: 300 }} key={i}>
                <CardItem bordered>
                    <Left>
                        <Thumbnail
                            source={
                                item.avatar
                                    ? { uri: item.avatar }
                                    : require('../assets/images/avatarph.png')
                            }
                        />
                        <Body>
                            <Text>{item.title}</Text>
                            <Text note>{`${item.time.hh}:${item.time.mm} ${
                                item.time.dd
                            }.${item.time.MM}.${item.time.yyyy}`}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem bordered>
                    <Body>
                        <Image
                            source={
                                item.imgHref
                                    ? { uri: item.imgHref }
                                    : require('../assets/images/eventph.png')
                            }
                            style={{ height: 125, width: '100%' }}
                        />
                        <Text>{item.description}</Text>
                    </Body>
                </CardItem>
                <CardItem bordered>
                    <Left>
                        <Button transparent textStyle={{ color: '#87838B' }}>
                            <Icon name="star" />
                            <Text>{item.stars} stars</Text>
                        </Button>
                    </Left>
                </CardItem>
            </Card>
        ));
    }
}
