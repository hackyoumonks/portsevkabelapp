import React from 'react';
import { Image, ScrollView, StyleSheet, View, Dimensions } from 'react-native';
import {
    Container,
    Content,
    Text,
    Body,
    Card,
    CardItem,
    Thumbnail,
    Button,
    Icon,
    Left,
    Right,
} from 'native-base';

import { Col, Row, Grid } from 'react-native-easy-grid';

const { width } = Dimensions.get('window');

export default class MapScreen extends React.Component {
    static navigationOptions = {
        title: 'Карта Порта',
    };

    render() {
        return (
            <Container>
                <Content>
                    <Grid>
                        <Col style={{ justifyContent: 'center', margin: 5 }}>
                            <View
                                style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    justifyContent: 'space-around',
                                    margin: 20,
                                }}
                            >
                                <Button
                                    onPress={() =>
                                        this.props.navigation.navigate(
                                            'HowToGo'
                                        )
                                    }
                                    style={{ margin: 10 }}
                                >
                                    <Text>Как добраться</Text>
                                </Button>
                                <Button
                                    style={{ margin: 10 }}
                                    onPress={() =>
                                        this.props.navigation.navigate('Plan')
                                    }
                                >
                                    <Text>План местности</Text>
                                </Button>
                            </View>
                        </Col>
                        <Col style={{ justifyContent: 'center', margin: 5 }}>
                            <View
                                style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    justifyContent: 'space-around',
                                    margin: 20,
                                }}
                            >
                                <Button
                                  
                                    light
                                    onPress={() =>
                                        this.props.navigation.navigate(
                                            'HowToGo'
                                        )
                                    }
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        margin: 5,
                                        width: 90, 
                                        height: 90,
                                    }}
                                >
                                    <Text>scan QR</Text>
                                </Button>
                            </View>
                        </Col>
                    </Grid>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});
