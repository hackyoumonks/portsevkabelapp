import React, { PureComponent } from 'react';
import {
    ScrollView,
    StyleSheet,
    View,
    Dimensions,
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Body,
    Button,
    Icon,
    Left,
    Right,
    Accordion 
} from 'native-base';


export default class EventScreen extends PureComponent {
    static navigationOptions = {
        header: null,
    };

    _renderItem({ item }) {
        return (
            <View style={styles.slide}>
                <Text style={styles.title}>{item.title}</Text>
            </View>
        );
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent>
                            <Icon name="back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Событие</Title>
                    </Body>
                    <Right />
                </Header>
                <Content>
                    <Text>Мастер-класс</Text>
                    <Text>Открытый мастер-класс по скейтбордингу</Text>
                    <Text>10 ноября 16:00-18:00</Text>
                    <Text>Профессиональный скейтбордист, тренер школы ONE 2 FLIP, расскажет и покажет основы скейтбординга, также каждый желающий сможет сделать первые шаги на скейте под руководством тренера. Вы узнаете о разных направлениях и стилях катания в скейтбординге. Ответим на часто задаваемые вопросы: с чего начать, как выбрать скейт, где кататься, где учиться?</Text>
                    <Text></Text>
            
                    <GalleryEvent eventid={} />

                    <Accordion dataArray={dataArray} expanded={0}/>

                    <Button transparent primary>
                        Пойду!
                    </Button>
                    <Button transparent>
                        Ищу компанию                    
                    </Button>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        //width: '100%',
        height: '100%',
    },
});

class GalleryEvent extends PureComponent {

  render() {
    return (
        <ScrollView horizontal={true}>
            <GalleryEvent eventid={} />
        </ScrollView>
    )
  }
}
