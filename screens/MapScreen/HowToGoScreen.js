import React from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    View,
    Dimensions,
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Body,
    Card,
    CardItem,
    Thumbnail,
    Button,
    Icon,
    Left,
} from 'native-base';

const { width, height } = Dimensions.get('window');

export default class HowToGoScreen extends React.Component {
    static navigationOptions = {
        title: 'Карта Порта',
    };

    render() {
        return (
            <ScrollView>
                <View
                    style={{
                        width: '80%',
                        marginTop: 10,
                        marginBottom: 10,
                        marginLeft: 'auto',
                        marginRight: 'auto',
                    }}
                >
                    <Text style={{ fontSize: 30, fontWeight: 'bold' }}>
                        Порт Севкабель
                    </Text>
                    <Text style={{ fontSize: 15, fontWeight: 'normal' }}>
                        КОЖЕВЕННАЯ ЛИНИЯ 40, ГАВАНЬ В.О., САНКТ-ПЕТЕРБУРГ
                    </Text>
                </View>
                <View
                    style={{
                        width: '90%',
                        marginTop: 10,
                        marginBottom: 10,
                        marginLeft: 'auto',
                        marginRight: 'auto',
                    }}
                >
                    <Text style={{ fontSize: 15, fontWeight: 'normal' }}>
                        Схема проезда
                    </Text>
                    <Image
                        source={require('../../assets/images/howtogo.png')}
                        style={{
                            margin: 15,
                            width: '90%',
                        }}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});
