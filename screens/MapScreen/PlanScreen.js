import React from 'react';
import { Image, ScrollView, StyleSheet, View, Dimensions } from 'react-native';
import {
    Container,
    Content,
    Text,
    Body,
    Card,
    CardItem,
    Thumbnail,
    Button,
    Icon,
    Left,
} from 'native-base';

const { width } = Dimensions.get('window');

export default class PlanScreen extends React.Component {
    static navigationOptions = {
        title: 'Карта Порта',
    };

    render() {
        return (
            <ScrollView style={styles.container}>
                {/* Go ahead and delete ExpoLinksView and replace it with your
                 * content, we just wanted to provide you with some helpful links */}
                <Image
                    source={require('../../assets/images/Frame.png')}
                    style={{ height: 210, width: width }}
                />
                

                <Card>
                    <CardItem
                        button
                        onPress={() => alert('План А')}
                    >
                        <Text>А Башня</Text>
                    </CardItem>
                    
                    <CardItem
                        button
                        onPress={() => alert('План Б')}
                    >
                        <Text>Б Кабельный цех</Text>
                    </CardItem>
                    <CardItem
                        button
                        onPress={() => alert('План Д')}
                    >
                        <Text>Д УКТ</Text>
                    </CardItem>
                    <CardItem
                        button
                        onPress={() => alert('План Е')}
                    >
                        <Text>Е НИИ</Text>
                    </CardItem>
                </Card>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});
