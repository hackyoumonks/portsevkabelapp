import React from 'react';
import { Platform } from 'react-native';
import {
    createStackNavigator,
    createBottomTabNavigator,
} from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';

import HomeScreen from '../screens/HomeScreen';
import AboutScreen from '../screens/AboutScreen';
import MapScreen from '../screens/MapScreen';
import ScheduleScreen from '../screens/ScheduleScreen';
import AccScreen from '../screens/AccScreen';
import HowToGoScreen from '../screens/MapScreen/HowToGoScreen';
import PlanScreen from '../screens/MapScreen/PlanScreen';


const HomeStack = createStackNavigator({ Home: HomeScreen });

HomeStack.navigationOptions = {
    tabBarLabel: 'Лента',
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={focused}
            name={Platform.OS === 'ios' ? 'ios-list' : 'md-list'}
        />
    ),
};

const MapStack = createStackNavigator(
    {
        Map: MapScreen,
        HowToGo: HowToGoScreen,
        Plan: PlanScreen
    },
    {
        initialRouteName: 'Map',
    }
);

MapStack.navigationOptions = {
    tabBarLabel: 'Карта',
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={focused}
            name={Platform.OS === 'ios' ? 'ios-map' : 'md-map'}
        />
    ),
};

const AboutStack = createStackNavigator({
    Settings: AboutScreen,
});

AboutStack.navigationOptions = {
    tabBarLabel: 'О нас',
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={focused}
            name={
                Platform.OS === 'ios'
                    ? `ios-information-circle${focused ? '' : '-outline'}`
                    : 'md-information-circle'
            }
        />
    ),
};

const ScheduleStack = createStackNavigator({
    Schedule: ScheduleScreen,
});

ScheduleStack.navigationOptions = {
    tabBarLabel: 'Расписание',
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={focused}
            name={Platform.OS === 'ios' ? 'ios-calendar' : 'md-calendar'}
        />
    ),
};

const AccountStack = createStackNavigator({
    Account: AccScreen,
});

AccountStack.navigationOptions = {
    tabBarLabel: 'Личный кабинет',
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={focused}
            name={Platform.OS === 'ios' ? 'ios-man' : 'md-man'}
        />
    ),
};

export default createBottomTabNavigator({
    HomeStack,
    AboutStack,
    MapStack,
    ScheduleStack,
    AccountStack,
});
